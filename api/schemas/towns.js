import mongoose from "mongoose";

const townSchema = new mongoose.Schema({
  details: {
    name: String,
    government: String,
    notablePlaces: String,
  },
  population: Array,
  user: String,
});

export const Towns = mongoose.model("Towns", townSchema);
