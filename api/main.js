import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import towns from "../api/routes/towns";
import user from "../api/routes/user";
import passport from "passport";
require("dotenv").config();

// const db = require("./config/keys").mongoURI;

mongoose.connect(
  `mongodb+srv://chDabney:4tDczW8rzsaKlqqM@cluster0.y9xpy.mongodb.net/denziensanddistricts?retryWrites=true&w=majority`,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  }
);

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());
require("./config/passport")(passport);

app.use("/api/towns", towns);
app.use("/api/user", user);

app.get("/api", (req, res) => {
  //handle root
  res.send("hello root");
});

export default app;
