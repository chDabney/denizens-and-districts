import express from "express";
import { Towns } from "../schemas/towns";

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const towns = await Towns.find({});
    res.status(200).send(towns);
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

router.get("/:user", async (req, res) => {
  try {
    const towns = await Towns.find({ user: req.params.user });
    res.status(200).send(towns);
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

router.get("/fluffernutter/:id", getId, async (req, res) => {
  try {
    const town = await Towns.findById(req.params.id);
    res.status(200).send(town);
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

router.post("/", async (req, res) => {
  try {
    const town = await Towns.create(req.body);
    res.status(201).send(town);
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

router.patch("/:id", getId, async (req, res) => {
  if (req.params.details !== null) {
    res.town.details = req.body.details;
  }

  try {
    const updateTown = await res.town.save();
    res.status(201).send(updateTown);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

router.delete("/:id", getId, async (req, res) => {
  try {
    await res.town.remove();
    res.json({ message: "Deleted" });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

async function getId(req, res, next) {
  let town;
  try {
    town = await Towns.findById(req.params.id);
    if (town === null) {
      return res.status(404).json({ message: "User not found" });
    }
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
  res.town = town;
  next();
}

export default router;
