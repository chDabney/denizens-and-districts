import useMagicLink from "use-magic-link";

export default function useAuth() {
  return useMagicLink("pk_test_1F8D517C1409902A");
}
