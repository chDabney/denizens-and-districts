# Debugs&Devs

Denizens and Districts is a tool for Dungeon masters to generate information about their worlds on the fly. If you are new or an old vet just wanting a quick set up, this site will help you flesh out a world sprawling with NPCS in an instant. 

## Project Requirements

Once your project has been completed please fill out the links below. These are a part of the rubric and must be completed before the submission deadline

**[Scrum Board URL](https://trello.com/b/Tac41db7/title-to-be-determined)** |
**[Deployment URL](https://kapstone.denziensanddistricts.vercel.app/)** |
**[Deployment Backup URL](https://kapstone.benscott92.vercel.app/)** |
**[Kapstone Pitch URL](https://docs.google.com/document/d/1pAkY6M3_myue8IfrM0OB1J45m3cAWQQyhUTrnJC88dk/edit)** |
**[Group Retrospective Document URL](https://docs.google.com/document/d/1bjxRfECwkpUoGXwFT9KslaMUg_lFXEAqIxTpuHkbC0Y/edit?usp=sharing)** |
**[Kapstone Feedback Form](https://docs.google.com/forms/d/e/1FAIpQLSeGbm0WcPzlEXHYkWDHcKmXr0fY3cF9sA1zGiP16sjd-0Jg5A/viewform)**

> **Note:** **Points will be deducted** if this is not filled in correctly. Also each one of these **MUST** live in their own document. For example DO **NOT** put your retrospective in your scrum board as it will not be counted and you will lose points.

## About Us

Tell us about your team, who was involved, what role did they fulfill and what did they work on (the more details the better). Also please choose ONE of the staff below that was your investor and remove the others.

| Name           | Scrum Role         | I worked on                      |
| -------------- | ------------------ | -------------------------------- |
| Zach           | `Product Investor` | Investing                        |
| Jason Hoyt     | `QA`               | User auth                        |
| Chad Dabney    | `Product Owner`    | Backend and user profile components, axios  |
| Benjamin Scott | `Scrum Master`     | Frontend, axios, town components |
| Justin Self    | `Dev`              | Navigation                       |
